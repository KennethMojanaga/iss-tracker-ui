FROM node:alpine
WORKDIR /app
COPY package.json ./
COPY package-lock.json ./

COPY ./ ./
RUN npm install

ENV DEFAULT_HOST=0.0.0.0

EXPOSE 3000

CMD ["npm", "run", "start"]