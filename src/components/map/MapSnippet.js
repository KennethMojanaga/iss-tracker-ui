import React, { useEffect, useState } from 'react';
import { MapContainer, TileLayer, Marker, Popup, Polyline } from 'react-leaflet';
import L from 'leaflet';
import space_sation_icon from '../../iss.svg';
import red_circle_icon from '../../red-circle.svg';

import { useSubscription } from "@apollo/client";
import { LOCATION_UPDATED_SUBSCRIPTION } from "../../graphql/subscriptions";

import "./MapSnippet.css";

const MapSnippet = () => {
    // Initialize state
    const [lat, setLat] = useState(0);
    const [long, setLong] = useState(0);

    const [currentLat, setCurrentLat] = useState(0);
    const [currentLong, setCurrentLong] = useState(0);

    const issHeightAboveEarth = 408; // in Km
    const [issDistance, setISSDistance] = useState(0);
    const [issBearing, setISSBearing] = useState(0);

    const [locationTrail, setlocationTrail] = useState([]);


    // Utility Functions
    function calculateDistanceBtPoints(lat1, lon1, lat2, lon2) {
        const p = 0.017453292519943295; // Math.PI / 180
        const c = Math.cos;
        const a = 0.5 - c((lat2 - lat1) * p)/2 + 
                c(lat1 * p) * c(lat2 * p) * 
                (1 - c((lon2 - lon1) * p))/2;
        return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
    }

    function calculateBearingBtPoints(lat1, lon1, lat2, lon2) {
        const y = Math.sin(lon2-lon1) * Math.cos(lat2);
        const x = Math.cos(lat1)*Math.sin(lat2) -
        Math.sin(lat1)*Math.cos(lat2)*Math.cos(lon2-lon1);
        const bearing = Math.atan2(y, x) * 180 / Math.PI;

        return bearing;
    }

    // Subscribe to backen GQL API for location updates
    const { data } = useSubscription(LOCATION_UPDATED_SUBSCRIPTION);

    // Update lat and long when ISS location subscription emits data
    useEffect(() => {
        if (data) {
            // Update trail
            const newLocationTrail = data.locationUpdated.location_trail;
            setlocationTrail(newLocationTrail);
            
            // Update current ISS location
            setLat(data.locationUpdated.latitude);
            setLong(data.locationUpdated.longitude);
        }
    }, [data]);

    // Re-render screen when lat and long change
    useEffect(() => {
        const apparentDistance = calculateDistanceBtPoints(lat, long, currentLat, currentLong);

        // Pythegoras formula (to get the actual distance)
        const tmp = Math.pow(apparentDistance, 2) + Math.pow(issHeightAboveEarth, 2);
        const actualDistance = Math.sqrt(tmp);

        // Calculate the bearing
        const bearing = calculateBearingBtPoints(lat, long, currentLat, currentLong)

        setISSDistance(Math.floor(actualDistance));
        setISSBearing(Math.floor(bearing))
    }, [lat, long]);

    // Component Data
    const zoom = 2;
    const contributors = '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';
    const spaceStationIcon = new L.Icon({
        iconUrl: space_sation_icon,
        iconRetinaUrl: space_sation_icon,
        popupAnchor: [-0, -0],
        iconSize: [50, 50]
    });

    const locationTrailIcon = new L.Icon({
        iconUrl: red_circle_icon,
        iconRetinaUrl: red_circle_icon,
        popupAnchor: [-0, -0],
        iconSize: [10, 10]
    });

    // Component Did Mount
    useEffect(() => {
        (async () => {
            // Extract GPS coodinates from browser
            if ("geolocation" in navigator) {
                // Geo-location tracking is available
                navigator.geolocation.getCurrentPosition(function(position) {
                    setCurrentLat(position.coords.latitude)
                    setCurrentLong(position.coords.longitude)
                });
            } else {
                alert('Please enable geo-location tracking in your browser in order for this application to work')
            }
        })();
    }, []);


    const lineOptions = { color: 'blue' };

    // Render
    return (
        <div style={{textAlign: "center"}}>
            {
                (lat === 0 && long === 0) ? (<h1>Loading...</h1>) :
                (
                    <>
                    <div style={{background:"#0f65b1", color:"white"}}>
                        <h3>Distance: {issDistance} KM, Bearing: {issBearing} Degrees</h3>
                        <h3>ISS Height Above Earth: {issHeightAboveEarth} KM</h3>
                    </div>
                    <MapContainer
                        center={[currentLat, currentLong]}
                        zoom={zoom} scrollWheelZoom={false}
                    >
                        
                        <TileLayer
                            url={"https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"}
                            attribution={contributors}
                        />
                        <Marker position={[lat, long]} icon={spaceStationIcon} >
                            <Popup>
                                <h3>Current Space Station Position:</h3>
                                Lat: {lat}, <br /> Long: {long}.
                            </Popup>
                        </Marker>

                        <>
                        {locationTrail.map((value, index) => {
                            return (
                                <Marker
                                    key={index}
                                    position={[value.latitude, value.longitude]}
                                    icon={locationTrailIcon}
                                >
                                    <Popup>
                                    <h3>Position at {value.datetime}:</h3>
                                    Lat: {value.latitude}, <br /> Long: {value.longitude}.
                                    </Popup>
                                </Marker>
                            )
                        })}
                        </>

                        <Marker position={[currentLat, currentLong]}>
                            <Popup>
                                <h3>Our Current Position:</h3>
                                Lat: {lat}, <br /> Long: {long}.
                            </Popup>
                        </Marker>

                        {/* LINE B/T CURRENT POSITION AND ISS */}
                        <Polyline pathOptions={lineOptions} positions={[[lat, long], [currentLat, currentLong]]}>
                        <Popup>
                            <h3>Line indicating the aparent distance between us and the ISS</h3>
                        </Popup>
                        </Polyline>
                    </MapContainer>
                    </>
                )
            }
        </div>
    );
};

export default MapSnippet;
