import React from 'react';
import './App.css';

// Custom Components
import MapSnippet from './components/map/MapSnippet';

import {
    ApolloClient,
    InMemoryCache,
    ApolloProvider,
    HttpLink,
    from,
    split
} from "@apollo/client";
import { getMainDefinition } from '@apollo/client/utilities';
import { onError } from "@apollo/client/link/error";
import { WebSocketLink } from '@apollo/client/link/ws';

const errorLink = onError(({graphqlErrors, networkError}) => {
    if (graphqlErrors) {
        graphqlErrors.map(({message, location, path}) => {
            alert(`Graphql error ${message}`);
        })
    }
});

const httpLink = from([
    errorLink,
    new HttpLink({uri: 'http://localhost:5050/graphql'})
]);

const wsLink = new WebSocketLink({
    uri: 'ws://localhost:5050/graphql',
    options: {
        reconnect: true
    }
});

const splitLink = split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
        );
    },
    wsLink,
    httpLink,
);

const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: splitLink
});

function App() {
    return (
        <ApolloProvider client={client}>
            <div className="App">
                <h1>International Space Station Tracking</h1>
                <MapSnippet />
            </div>
        </ApolloProvider>
    );
}

export default App;
