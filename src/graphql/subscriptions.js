import { gql } from "@apollo/client";

export const LOCATION_UPDATED_SUBSCRIPTION = gql`
    subscription {
        locationUpdated {
            id
            datetime
            latitude
            longitude
            location_trail {
                id
                datetime
                latitude
                longitude
            }
        }
    }
`;